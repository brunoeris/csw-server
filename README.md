# README

This README would normally document whatever steps are necessary to get the
application up and running.


* Ruby version 2.3.0

* Rails version 5.0.0

* Database Postgresql

### Setup

1. Clone this repository
2. bundle install
3. rails db:create
4. rails db:migrate
5. rails s
