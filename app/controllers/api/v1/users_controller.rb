class Api::V1::UsersController < Api::V1::BaseController
  skip_before_action :doorkeeper_authorize!, only: [:create]
  before_action :set_user, only: [:me] 

  def me
  end 

  def create
    @user = User.create(user_params)
    if @user.persisted?
      generate_doorkeeper_access_token(@user)
      render :show, status: :created
    else
      render_unprocessable_entity_response_with_message(@user.errors.full_messages)
    end
  end

  private
  def user_params
    params.require(:user).permit(:id, :full_name, :email, :password)
  end  

  def generate_doorkeeper_access_token(user)
    application = Doorkeeper::Application.first
    Doorkeeper::AccessToken.create!(application_id: application.id, resource_owner_id: user.id)
  end

  def set_user
    @user = current_user
  end
end
