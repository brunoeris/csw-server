class Api::V1::BaseController < ActionController::API
  respond_to :json

  before_action :doorkeeper_authorize!

  def render_unprocessable_entity_response_with_message(message)
    render json: message, status: :unprocessable_entity
  end

  def current_user
    User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end  
end
