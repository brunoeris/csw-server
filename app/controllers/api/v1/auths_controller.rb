class Api::V1::AuthsController < Doorkeeper::TokensController
  def create
    user = User.where(email: params[:email]).first
    if user
      super
    else
      render json: [ "Usuário não identificado", status: :unprocessable_entity ]
    end
  end
end
