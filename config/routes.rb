Rails.application.routes.draw do
  use_doorkeeper
  devise_for :users, controllers: { confirmations: 'users/confirmations' }
  root to: 'visitors#index'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resource :users, only: [:create] do
        collection do
          get 'me'
        end
      end

      resource :auth, only: [:create]
    end
  end
end
